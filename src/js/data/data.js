/**
 * @name data
 * @description Metadata of rules logic
 */
const data = {
  total: 0,
  iterator: 2,
  divider: 3,
  incrementer: 4,
  substractor: 2,
  modulo: 2
};

export default data;
